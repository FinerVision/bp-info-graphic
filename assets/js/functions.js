var preloadImages = function (images, callback) {
    var loaded = 0;

    if (callback !== undefined) {
        var interval = setInterval(function () {
            if (loaded === images.length) {
                clearInterval(interval);
                callback();
            }
        }, 100);
    }

    for (var i = 0; i < images.length; i++) {
        var image = new Image;
        image.src = images[i];
        image.onload = function () {
            loaded++;
        };
    }
};