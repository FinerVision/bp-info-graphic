var Share = function() {

    var that = this;

    this.listen = function(share) {
        that.share = share;

        var elements = document.getElementsByClassName('share-button');

        for(var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', function(e) {
                e.preventDefault();

                that[this.dataset.share](this);
            });
        }
    };

    this.destroy = function() {
        var elements = document.getElementsByClassName('share-button');

        for(var i = 0; i < elements.length; i++) {
            elements[i].removeEventListener('click', null);
        }
    };

    this.facebook = function(element) {
        var share = that.share['facebook'] !== undefined ? that.share['facebook'] : that.share['generic'];

        FB.ui({
            method: 'feed',
            name: share.name,
            link: share.link,
            caption: share.caption,
            picture: share.picture,
            description: share.description
        }, function(response) {
            if(response && response.post_id) {
                shared({
                    vendor: 'facebook',
                    response: response
                });
            }
        });
    };

    this.twitter = function(element) {
        var share = that.share['twitter'] !== undefined ? that.share['twitter'] : that.share['generic'];

        twttr.events.bind('tweet', function(event) {
            shared({
                vendor: 'twitter',
                response: event
            });
        });

        element.href = 'https://twitter.com/intent/tweet?url=' + share.link + '&text=' + share.text;
    };

    this.pinterest = function(element) {
        var href = 'https://www.pinterest.com/pin/create/bookmarklet/?url=' + that.share.link + '&media=' + that.share.image + '&description=' + that.share.description;

        window.open(href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=750,height=660');
    };

    this.linkedin = function(element) {
        var share = that.share['linkedin'] !== undefined ? that.share['linkedin'] : that.share['generic'];
        var href = 'https://www.linkedin.com/shareArticle?mini=true&url=' + share.link + '&title=' + share.title + '&summary=' + share.caption + '&source=' + share.link;

        window.open(href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=750,height=660');
    };

    var shared = function(object) {
        console.log(object);
    };

};