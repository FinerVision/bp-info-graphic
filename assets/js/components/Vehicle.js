var directions = [
    { class:"down" },
    { class:"left" },
    { class:"up" },
    { class:"right" }
];

var Vehicle = function () {

    var that = this;
    var stopped = false;

    this.frameContainer = [
        [
            {animation: {top: 0, left: 443}, duration: 0, direction: 0 },
            {animation: {top: 206, left: 443}, duration: 206, direction: 0 },

            {animation: {top: 240, left: 423}, duration: 0, direction: 1 },
            {animation: {top: 240, left: 173}, duration: 250, direction: 1 },
            {animation: {top: 261, left: 144}, duration: 0, direction: 0 },
            {animation: {top: 1453, left: 140}, duration: 1192.0067113905022, direction: 0 },
            {animation: {top: 1455, left: 157}, duration: 0, direction: 3 },
            {animation: {top: 1455, left: 926}, duration: 800, direction: 3 },

            {animation: {top: 1420, left: 954}, duration: 0, direction: 2 },
            {animation: {top: 287, left: 955}, duration: 1133.0004413061806, direction: 2 },
            {animation: {top: 235, left: 931}, duration: 0, direction: 1 },
            {animation: {top: 236, left: 721}, duration: 210.00238093888365, direction: 1 },
            {animation: {top: 203, left: 684}, duration: 0, direction: 2 },
            {animation: {top: -7, left: 684}, duration: 210, direction: 2 },
            //{animation: {top: -7, left: 714}, duration: 0, direction: 0 },
            {animation: {top: -7, left: 717}, duration: 0, direction: 0 },
            {animation: {top: 166, left: 717}, duration: 173, direction: 0 },
            {animation: {top: 203, left: 754}, duration: 0, direction: 3 },
            {animation: {top: 203, left: 959}, duration: 205, direction: 3 },
            {animation: {top: 249, left: 994}, duration: 0, direction: 0 },
            {animation: {top: 1426, left: 993}, duration: 1177.0004248087594, direction: 0 },
            {animation: {top: 1488, left: 953}, duration: 0, direction: 1 },
            {animation: {top: 1489, left: 155}, duration: 798.00062656617, direction: 1 },
            {animation: {top: 1526, left: 139}, duration: 0, direction: 0 },
            {animation: {top: 2606, left: 139}, duration: 1080, direction: 0 },
            {animation: {top: 2656, left: 173}, duration: 0, direction: 3 },
            {animation: {top: 2657, left: 992}, duration: 819.000610500383, direction: 3 },
            {animation: {top: 2698, left: 1030}, duration: 0, direction: 0 },
            {animation: {top: 3717, left: 1031}, duration: 1019.0004906770163, direction: 0 },
            {animation: {top: 3777, left: 981}, duration: 0, direction: 1 },
            {animation: {top: 3777, left: 181}, duration: 800, direction: 1 },
            {animation: {top: 3818, left: 159}, duration: 0, direction: 0 },
            {animation: {top: 4408, left: 159}, duration: 590, direction: 0 },
            {animation: {top: 4443, left: 185}, duration: 0, direction: 3 },
            {animation: {top: 4443, left: 925}, duration: 740, direction: 3 },
            {animation: {top: 4474, left: 969}, duration: 0, direction: 0 },
            {animation: {top: 4520, left: 969}, duration: 46, direction: 0 },
            {animation: {top: 4564, left: 941}, duration: 0, direction: 1 },
            {animation: {top: 4564, left: 237}, duration: 704, direction: 1 },
            {animation: {top: 4540, left: 188}, duration: 0, direction: 2 },
            {animation: {top: 4514, left: 188}, duration: 26, direction: 2 },
            {animation: {top: 4514, left: 191}, duration: 3, direction: 2 },
            {animation: {top: 4482, left: 191}, duration: 0, direction: 1 },
            {animation: {top: 4478, left: 155}, duration: 36.22154055254967, direction: 1 },
            {animation: {top: 4426, left: 114}, duration: 0, direction: 2 },
            {animation: {top: 3790, left: 114}, duration: 636, direction: 2 },
            {animation: {top: 3723, left: 171}, duration: 0, direction: 3 },
            {animation: {top: 3723, left: 947}, duration: 776, direction: 3 },
            {animation: {top: 3674, left: 974}, duration: 0, direction: 2 },
            {animation: {top: 2754, left: 974}, duration: 920, direction: 2 },
            {animation: {top: 2699, left: 934}, duration: 0, direction: 1 },
            {animation: {top: 2699, left: 144}, duration: 790, direction: 1 },
            {animation: {top: 2645, left: 97}, duration: 0, direction: 2 },
            {animation: {top: 261, left: 105}, duration: 2384.0134227810045, direction: 2 },
            {animation: {top: 206, left: 133}, duration: 0, direction: 3 },
            {animation: {top: 206, left: 383}, duration: 250, direction: 3 },
            {animation: {top: 168, left: 412}, duration: 0, direction: 2 },
            {animation: {top: -2, left: 412}, duration: 170, direction: 2 }
        ],[
            {animation: {top: 0, left: -114}, duration: 0, direction: 3 },
            {animation: {top: 0, left: 396}, duration: 510, direction: 3 },
            {animation: {top: 0, left: 906}, duration: 510, direction: 3 },
            {animation: {top: 0, left: 1416}, duration: 510, direction: 3 },
            {animation: {top: 0, left: 1926}, duration: 510, direction: 3 },

            {animation: {top: 25, left: 1926}, duration: 0, direction: 1 },

            {animation: {top: 25, left: 1416}, duration: 510, direction: 1 },
            {animation: {top: 25, left: 906}, duration: 510, direction: 1 },
            {animation: {top: 25, left: 396}, duration: 510, direction: 1 },
            {animation: {top: 25, left: -104}, duration: 510, direction: 1 }
        ]
    ];

    this.frames= [];

    this.setFrames = function (ind) {
        this.frames = this.frameContainer[ind];
    };

    this.initElement = function (element,index) {
        if (index >= that.frames.length) {
            index = 0;
        }

        if (index <0 ) {
            index=that.frames.length-1;
        }

        element.css(that.frames[index].animation);
    };

    this.animate = function (element, index) {
        animateFrame(element, index);
    };

    this.stop = function() {
        stopped = true;
    };

    var animateFrame = function (element, index) {
        if (index >= that.frames.length) {
            index = 0;
        }

        if (index <0 ) {
            index=that.frames.length-1;
        }

        if (that.frames[index].duration === 0) {
            element.css(that.frames[index].animation);
            if (!stopped) {
                if(!isAnimationStopped) animateFrame(element, index + 1);
            }
        } else {
            element.removeClass('up').removeClass('down').removeClass('left').removeClass('right');
            element.addClass(directions[that.frames[index].direction].class);
            element.velocity(that.frames[index].animation, {
                queue: false,
                duration: that.frames[index].duration * 5 || 0,
                easing: that.frames[index].easing || 'linear',
                complete: function () {
                    if (!stopped) {
                        if(!isAnimationStopped) animateFrame(element, index + 1);
                    }
                }
            });
        }
    };

};