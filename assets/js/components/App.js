var isAnimationStopped=false;

var App = function () {

    var that = this;

    var callable = [];
    this.globalScroll = false;

    var scrolling = false;

    this.init = function() {
        events();
    };

    this.parallax = function (elements, callback, visible) {
        visible = visible || [];

        $.each(elements, function () {
            callable.push({
                el: $(this),
                visible: [
                    visible[0] !== undefined,
                    visible[1] !== undefined
                ],
                cb: callback
            });
        });

        parallaxVisibleElements();
    };
    
    this.destroy = function() {
        callable = [];
        $(window).off('scroll.parallax');
        $(document).off('click.parallax');
    };

    var throttle = function (ms, callback) {
        var timer, lastCall = 0;

        return function () {
            var now = new Date().getTime(),
                diff = now - lastCall;
            if (diff >= ms) {
                lastCall = now;
                callback.apply(this, arguments);
            }
        };
    };

    var parallaxVisibleElements = function (e) {
        callable.map(function (object) {
            if (object.el.visible(object.visible[0], object.visible[1]) && object['animating'] !== true) {
                object['animating'] = true;
                object.cb(object.el);
            }
        });
    };

    var scrollTop = function (e) {

        if (!e.type || region) {
            if (!scrolling) {
                scrolling = true;

                $('html, body').animate({scrollTop: 0}, {
                    complete: function () {
                        scrolling = false;
                    }
                });
            }
        }
    };

    var events = function () {
        $(window).on('scroll.parallax', throttle(30, parallaxVisibleElements));
        $(document).on('click.parallax', '.bottom-link', scrollTop);
        $(document).on('scroll',function() { that.globalScroll = true; });
    };

};