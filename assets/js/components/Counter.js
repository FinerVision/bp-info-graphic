var Counter = function() {

    var $this = this;

    var $animated = false;

    var $options = {};

    var $countable;

    $this.init = function(options) {
        $options = options;
        $countable = $('[data-to]');

        reset();
        startCounting();
        events();
    };

    var startCounting = function() {
        var visible = checkVisibility();

        if (visible && !$animated) {
            $animated = true;

            $.each($countable, function() {
                var self = $(this);
                var from = self.data('from');
                var to = self.data('to');
                var type = self.data('type') || 'int';
                var decimalPlaces = self.data('decimals') || 0;
                var prefix = self.data('prefix') || '';
                var postfix = self.data('postfix') || '';

                $({
                    countNum: from
                }).delay($options.delay || 0).animate({
                    countNum: to
                }, {
                    duration: $options.duration || 1000,
                    easing: 'linear',
                    step: function() {
                        if (type === 'decimal') {
                            self.html(prefix + (Math.round(this.countNum * decimalPlaces) / decimalPlaces) + postfix);
                        } else {
                            self.html(prefix + (Math.floor(this.countNum)) + postfix);
                        }
                    },
                    complete: function() {
                        self.html(prefix + this.countNum + postfix);
                    }
                });
            });
        }
    };

    var reset = function() {
        $.each($countable, function() {
            $(this).text(0);
        });
    };

    var checkVisibility = function() {
        var windowViewTop = $(window).scrollTop();
        var windowViewBottom = windowViewTop + $(window).height();

        var containerTop = $options.container.offset().top;
        var containerBottom = containerTop + $options.container.height();

        return ((containerBottom <= windowViewBottom) && (containerTop >= windowViewTop));
    };

    var events = function() {
        $(window).on('scroll', startCounting);
    };

};