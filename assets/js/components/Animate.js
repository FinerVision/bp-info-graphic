var Animate = function () {

    var $this = this;

    var $objects = [];

    var $active = {
        person: false,
        trees: false
    };

    this.scrolled = false;

    $this.handshake = function (leftHand, rightHand, rightPalm, limit, callback) {
        limit = limit || 5;
        var count = 0;

        var loop = function () {
            leftHand.animate({transform: 't0,-15r4,0,0'}, 500);
            rightHand.animate({transform: 't-38,7r-3,0,0'}, 500);
            rightPalm.animate({transform: 't-38,7r-3,0,0'}, 500, null, function () {
                leftHand.animate({transform: 't0,0r0,0,0'}, 500);
                rightHand.animate({transform: 't0,0r0,0,0'}, 500);
                rightPalm.animate({transform: 't0,0r0,0,0'}, 500, null, function () {
                    if (count < limit) {
                        loop();
                        count++;
                    } else {
                        callback();
                    }
                });
            });
        };

        if (!isAnimationStopped) loop();
    };

    $this.pulse = function (element, duration) {
        element.velocity({opacity: 0.3}, {
            duration: duration,
            complete: function () {
                element.velocity({opacity: 1}, {
                    duration: duration,
                    complete: function () {
                        setTimeout(function () {
                            if(!isAnimationStopped) $this.pulse(element, duration);
                        }, 400);
                    }
                })
            }
        });
    };

    $this.cogs = function (time, repeat) {

        var selector = '.cog-feeder';
        if ($(window).width()<768) {
            $(selector).css("left","55%");
            var selector = '.cog-feederX';
        }

        if (!window.cogsAnimating) {
            $(selector).css({left: "-13%"});
        }

        window.cogsAnimating = true;
        repeat = repeat || false;

        var rotating = $('.rotating');

        //return false;
        for (var i = 0; i < rotating.length; i++) {
            var degrees = (i % 2 == 0) ? '105deg' : '-105deg';

            $(rotating[i]).velocity({rotateZ: repeat ? '0deg' : degrees}, {
                queue: false,
                duration: time,
                easing: 'linear'
            });
        }
        

        $(selector).velocity({left: repeat ? "-13%" : "10.3%"}, {

            queue: false,
            duration: time,
            easing: 'linear',
            complete: function () {
                if(!isAnimationStopped) $this.cogs(time, !repeat);
            }
        });
    };

    $this.dinger = function () {
        var ball = Snap('#Ball');
        var mallet = Snap('#Mallet');
        var quotes = Snap('#Quotes');

        if (mallet !== null) {
            setTimeout(function () {
                mallet.animate({transform: 'r0,t0,0'}, 250, null, function () {
                    ball.animate({cy: 110}, 450, mina.easeout, function () {
                        quotes.animate({transform: 'r-2,t5,7'}, 200);

                        setInterval(function () {
                            if(!isAnimationStopped) quotes.animate({transform: 'r2,t0,7'}, 200, null, function () {
                                quotes.animate({transform: 'r-2,t5,7'}, 200);
                            });
                        }, 400);
                    });
                });
            }, 800);
        }
    };

    $this.scale = function (element) {
        $({deg: from}).velocity({deg: to}, {
            duration: duration,
            step: function (now) {
                element.css({transform: 'rotate(' + now + 'deg)'});
            },
            complete: callback
        });
    };

    $this.growCity = function (city) {
        var buildings = [];

        for (var i = 1; i < 20; i++) {
            var building = Snap('#Building_' + i);

            if (building !== null) {
                building.attr({transform: 't0,240'});
                buildings.push(building);
            }
        }

        city.attr({transform: 't0,0'});

        buildings.map(function (building, index) {
            setTimeout(function () {
                building.animate({transform: 't0,0'}, 650);
            }, (index * 37) - (index * 10));
        });
    };

    $this.count = function (object) {
        object.to = object.to || object.element.attr('data-to');

        if (object.to > 0) {
            object.complete = object.complete || null;
            object.from = object.from || 0;
            object.delay = object.delay || 0;

            var decimal = object.to.split('.').length > 1 ? 1 : 0;

            var counter = new CountUp(
                object.element,
                object.from,
                object.to,
                decimal,
                object.duration || 1.5,
                'linear'
            );

            setTimeout(function () {
                counter.start(function () {
                    if (object.complete !== null) {
                        object.complete();
                    }
                });
            }, object.delay);
        }
    };

    $this.pulseate = function () {
        $('#scroll-down').fadeIn();

        $(window).on('scroll.pulsate', function () {
            throttle(30, function () {
                if (!$this.scrolled) {
                    $this.scrolled = true;
                    $('#scroll-down').removeClass('bounce');
                }
            });
        });
    };

    $this.destroy = function () {
        $(window).off('scroll.pulsate');
    };

    var animateFrame = function (frames, index) {
        if (index >= frames.length) {
            index = 0;
        }

        if (frames[index].duration === 0) {
            frames[index].element.attr(frames[index].animation);
            animateFrame(frames, index + 1);
        } else {
            frames[index].element.animate(
                frames[index].animation,
                frames[index].duration,
                frames[index].easing,
                animateFrame.bind(null, frames, index + 1)
            );
        }
    };

    $this.person = function () {
        $objects = [];

        for (var i = 1; i < 14; i++) {
            $objects.push({
                element: $('#Person_' + i),
                animated: false,
                animating: false
            });

            $objects[(i - 1)].element.stop().velocity({
                translateX: 142,
                translateY: -30,
                scale: 0.6,
                opacity: 0,
                transformOrigin: '50% 100%'
            }, 0, 'linear');
        }

        var timeout = (4800 / $objects.length);

        var conveyorAnimation = function (index) {
            setTimeout(function () {
                if (index === ($objects.length - 1)) {
                    conveyorAnimation(0);
                } else {
                    conveyorAnimation((index + 1));
                }

                if (!$objects[index].animating) {
                    $objects[index].animating = true;

                    $objects[index].element.stop().velocity({
                        translateX: 142,
                        translateY: -38,
                        scale: 1,
                        opacity: 1
                    }, 1000, 'linear')
                        .velocity({translateX: 265, translateY: -38}, 750, 'linear')
                        .velocity({translateX: 320, translateY: 0}, 150, 'linear')
                        .velocity({translateX: 750, translateY: 0}, 2300, 'linear')
                        .velocity({translateX: 800, translateY: 0, opacity: 0}, 300, 'linear', function () {
                            $objects[index].element.stop().velocity({
                                translateX: 142,
                                translateY: -30,
                                scale: 0.6,
                                opacity: 0
                            }, 0, 'linear');
                            $objects[index].animating = false;
                        });
                }
            }, timeout);
        };

        if (!$active.person) {
            $active.person = true;
            conveyorAnimation(0);
        }
    };

    var throttle = function (ms, callback) {
        var timer, lastCall = 0;

        return function () {
            var now = new Date().getTime(),
                diff = now - lastCall;
            if (diff >= ms) {
                lastCall = now;
                callback.apply(this, arguments);
            }
        };
    };

    var rand = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };

    var rotate = function (element, from, to, duration, callback) {
        callback = callback || null;

        $({deg: from}).velocity({deg: to}, {
            duration: duration,
            step: function (now) {
                element.css({transform: 'rotate(' + now + 'deg)'});
            },
            complete: callback
        });
    };

};