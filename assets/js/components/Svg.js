var Svg = function () {

    var $this = this;

    var $svgs = {
        map: {
            snap: Snap('#mapSvg'),
            elements: {}
        }
    };

    $this.init = function () {
        events();
    };

    $this.initializeRegion = function (region, callback) {
        $this.region = region;
        $this.callback = callback;

        showSelectedRegion();
    };

    $this.destroy = function() {
        $(document).off('click.svg');
        $(document).off('mouseover.svg');
        $(document).off('mouseout.svg');
    };

    $this.resetMap = function() {
        $this.region = '';

        $('#intro').find('.btn-primary').animate({opacity: 1, width: 580, marginLeft: 15},
        function() { $('#intro').find('.btn-primary').css({left:0, right:'auto',marginLeft: 15}); }
        ).find('span').html(
            'To discover BP\'s contribution to your area, please select your location on the map &nbsp;&nbsp;&nbsp;&nbsp;' +
            '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAXCAYAAAA/ZK6/AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDozNkRBRDBFRjI4MzMxMUUzQjdEODkzOTM4RjgwNTA4QSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDozNkRBRDBGMDI4MzMxMUUzQjdEODkzOTM4RjgwNTA4QSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjM2REFEMEVEMjgzMzExRTNCN0Q4OTM5MzhGODA1MDhBIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjM2REFEMEVFMjgzMzExRTNCN0Q4OTM5MzhGODA1MDhBIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+ga3geQAAAKpJREFUeNqUkssJwkAURV8uwbK0BwlknY0dWIwVGESLMBtRXFuGBbgQ74ADL3Fmch045MM5E/KSane3rZldyWDCArmRE1mqwZk0aoTvUY7gzqUIk2sfrZTAR8dUhMyTsxEK75eMMDPFnwjCtxpFShDWhTzIRgkWZE+epIMoB68lL/wjh5t1Qe5J5eXclLJyKijK02BW9oEkx0CWY7BW5TjWsPuBvJV/5CPAAGqqL0CMEdXsAAAAAElFTkSuQmCC">' +
            '&nbsp;&nbsp;'
        );


        $('[data-region]').attr('class', 'region');

        $svgs.map.elements.line.attr({width: 0, height: 0});
        $svgs.map.elements.line_left.attr({width: 0, height: 0});
    };

    var showSelectedRegion = function () {

        var region = $(this).data('region') || $this.region;

        if (Object.keys(($svgs.map.elements)).length === 0) {
            $svgs.map.elements['line'] = $svgs.map.snap.rect(0, 0, 0, 0);
            $svgs.map.elements['line_left'] = $svgs.map.snap.rect(0, 0, 0, 0);
        }

        selectRegion(region);
    };

    var selectRegion = function (region) {

        for (var i = 0; i < Region.regions.length; i++) {
            if (Region.regions[i].id === region) {
                region = Region.regions[i];
                $svgs.map.region = region;
            }
        }

        if ($(window).width() <= 768) {
            $svgs.map.elements.line.attr({width: 0, height: 0});
            $svgs.map.elements.line_left.attr({width: 0, height: 0});

            if ($this.callback !== undefined) {
                $this.callback();
            }

            return;
        }

        $svgs.map.snap.g($svgs.map.elements.line);

        $svgs.map.elements.line.attr({fill: '#176b36'});
        $svgs.map.elements.line_left.attr({fill: '#176b36'});

        $('.region').attr('class', 'region');
        $('[data-region="' + region.id + '"]').attr('class', 'region selected');

        $svgs.map.elements.line.stop().attr({
            x: region.x - 105,
            y: region.y,
            width: 6,
            height: 0
        });
        $svgs.map.elements.line_left.stop().attr({
            x: region.x - 105,
            y: 174,
            width: 0,
            height: 6
        });

        var select = $('.select').find('span');

        select.fadeOut(1100);

        select.parent().animate({opacity: 0}, 550, function () {
            var that = $(this);
            that.css({width: 'auto', left: 'auto', right: 0, marginLeft: 0});

            $svgs.map.elements.line.animate({y: 174, height: region.y - 174}, 550, null, function () {

                $svgs.map.elements.line_left.animate({x: -100, width: region.x}, 550, null, function () {
                    that.animate({opacity: 1}, 550, function () {
                        select.text(region.name).fadeIn(350);
                        setTimeout(function () {
                            if ($this.callback !== undefined) {
                                $this.callback();
                            }
                        }, 500);
                    });
                });

            });
        });


    };

    var darkenRegion = function () {
        $('[data-region="' + $(this).data('region') + '"]').stop().animate({svgFill: '#176b36'});
    };

    var lightenRegion = function () {
        $('[data-region="' + $(this).data('region') + '"]').stop().animate({svgFill: '#86BC25'});
    };

    var events = function () {
        $(document).on('click.svg', '.region', showSelectedRegion);
        $(document).on('mouseover.svg', '.region', darkenRegion);
        $(document).on('mouseout.svg', '.region', lightenRegion);
    };

};