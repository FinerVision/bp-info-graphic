$(document).on('click', '#mobile-menu', function() {
    var menuButton = $(this);
    var nav = $('nav');

    if (menuButton.hasClass('active'))
    {
        menuButton.removeClass('active');
        nav.fadeOut();
    }
    else
    {
        menuButton.addClass('active');
        nav.fadeIn();
    }
});