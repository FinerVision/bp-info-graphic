var bpControllers = angular.module('bpControllers', []);

bpControllers.controller('AppController', ['$scope', '$location', function ($scope, $location) {
    $scope.regions = Region.regions;
    $('.scroll-down').removeClass('visible-xs');
    $('.scroll-down').addClass('hidden');
    var cards = [];

    $scope.regions.forEach(function (region) {
        cards.push('assets/img/cards/' + region.id + '.png');
    });

    preloadImages(cards);

    $scope.go = function (path) {
        if (path === '/') {
            window.svg.resetMap();
        }

        $location.path(path);
    }
}]);

bpControllers.controller('RegionController', ['$scope', '$filter', '$routeParams', function ($scope, $filter, $routeParams) {
    GlobalScroll = false;
    $scope.region = $filter('findRegion')(Region.regions, $routeParams.region);
    $scope.road = $scope.region.id === 'london' ? 'london' : 'united_kingdom';
    $('.scroll-down').removeClass('hidden');
    $('.scroll-down').addClass('visible-xs');

    var app = new App();
    var ve = new Vehicle();
    var ve2 = new Vehicle();

    ve.setFrames(0);
    ve2.setFrames(1);

    window.animate.undefined = undefined;

    document.title = 'BP: ' + $scope.region.name;

    $('.region').attr('class', 'region');
    $('[data-region="' + $scope.region.id + '"]').attr('class', 'region selected');
    $('.text-fluid').find('h1').fitText(1.1, {minFontSize: '19px', maxFontSize: '44px'});

    var animationEnded = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    var card = $('.animate-flipInX');
    var banner = $('.banner').find('.content');
    var centerCard = $('.center-card');
    var handshake = $('#handshake');
    var leftHand = Snap('#Left_Hand');
    var rightHand = Snap('#Right_Hand');
    var rightPalm = Snap('#Right_Palm');
    var cityInHandGroup = Snap('#City_In_Hand_Group');
    var city = Snap('#City');
    var region = $('#region');
    var mallet = $('#Mallet');


    // Counters
    var annualContribution = {el: $('#annual_contribution'), animate: new Animate};
    var indirectImpact = {el: $('#indirect_impact'), animate: new Animate};
    var directContribution = {el: $('#direct_contribution'), animate: new Animate};
    var suppliers = {el: $('#suppliers'), animate: new Animate};
    var inducedImpact = {el: $('#induced_impact'), animate: new Animate};
    var jobs = {el: $('#jobs'), animate: new Animate};

    leftHand.attr({transform: 't-850,0'});
    rightHand.attr({transform: 't850,0'});
    rightPalm.attr({transform: 't850,0'});
    cityInHandGroup.attr({transform: 't900,0'});
    city.attr({transform: 't0,220'});
    region.css({height: 0, overflow: 'hidden'});

    //console.log('Region initialized.');

    app.init();

    window.share.listen({
        generic: {
            name: 'BP in ' + $scope.region.name,
            link: document.baseURI + $scope.region.id,
            caption: 'Committed to Britain',
            picture: document.baseURI + 'assets/img/share_logo.png',
            description: "Discover BP's contribution to your region and the rest of the UK.",
            image: document.baseURI + 'assets/img/downloads/pngs/' + $scope.region.id + '.png'
        },
        twitter: {
            link: document.baseURI + $scope.region.id,
            text: "Discover BP's contribution to " + $scope.region.name + " and the rest of the UK here "
        },
        linkedin: {
            link: document.baseURI + $scope.region.id,
            title: 'BP in ' + $scope.region.name,
            caption: 'Committed to Britain',
            text: "Discover BP's contribution to " + $scope.region.name + " and the rest of the UK here "
        }
    });

    region.css({height: 'auto', overflow: 'initial'});

    window.svg.initializeRegion($scope.region);


    setTimeout(function () {
        if (!app.globalScroll)
            $('html, body').ready().animate({scrollTop: region.offset().top}, 850, function () {
                setTimeout(function () { window.animate; }.pulseate, 200);
            });
    },2350)

    window.animate.person();

    if (!window.cogsAnimating) {
        window.animate.cogs(3800);
    }

    app.parallax(card, function (element) {
        element.addClass('flipInX');
        element.one(animationEnded, function () {
            element.css({transform: 'rotateX(0deg)'}).removeClass('flipInX').find('img').addClass('pulse');
        });
    });

    app.parallax(annualContribution.el, function (element) {
        annualContribution.animate.count({delay: 600, element: element[0], to: element.attr('data-to')});
    });

    app.parallax(indirectImpact.el, function (element) {
        indirectImpact.animate.count({delay: 250, element: element[0], to: element.attr('data-to'), duration: 1});
    });

    app.parallax(directContribution.el, function (element) {
        directContribution.animate.count({
            delay: 150,
            element: element[0],
            to: element.attr('data-to'),
            duration: 1
        });
    });

    app.parallax(suppliers.el, function (element) {
        suppliers.animate.count({delay: 150, element: element[0], to: element.attr('data-to'), duration: 1});
    });

    app.parallax(inducedImpact.el, function (element) {
        inducedImpact.animate.count({element: element[0], to: element.attr('data-to'), duration: 1});
    });

    app.parallax(jobs.el, function (element) {
        jobs.animate.count({element: element[0], to: element.attr('data-to'), duration: 1});
    });

    app.parallax(banner, function (element) {
        element.find('.first-flipInY').addClass('flipInY');
        element.find('.first-flipInY').one(animationEnded, function () {
            element.find('.second-flipInY').addClass('flipInY');
        });
    });

    app.parallax(centerCard, function (element) {
        element.find('.green-container').addClass('flipInX');
        element.find('.green-container').one(animationEnded, function () {
            element.find('.first-flipInY').addClass('flipInY');
            element.find('.first-flipInY').one(animationEnded, function () {
                element.find('.second-flipInY').addClass('flipInY');
            });
        });
    });

    app.parallax(handshake, function (element) {
        setTimeout(function () {
            leftHand.animate({transform: 't0,0'}, 750);
            rightHand.animate({transform: 't0,0'}, 750);
            rightPalm.animate({transform: 't0,0'}, 750, function () {
                element.find('.sun-glow').fadeIn(450, function () {
                    animate.pulse(element.find('.sun-glow'), 1000);
                });

                window.animate.handshake(leftHand, rightHand, rightPalm, 1, function () {
                    leftHand.animate({transform: 't-850,0'}, 500);
                    rightHand.animate({transform: 't850,0'}, 500);
                    rightPalm.animate({transform: 't850,0'}, 500, null, function () {
                        element.find('.sun-glow').fadeOut(450);

                        cityInHandGroup.animate({transform: 't0,0'}, 500, null, function () {
                            window.animate.growCity(city);
                        });
                    });
                });
            });
        }, 500);
    }, [true]);

    app.parallax(mallet, function () {
        window.animate.dinger();
    });

    $scope.$on('$destroy', function () {
        app.destroy();
        ve.stop();
        window.animate.destroy();
        $(document).off('click', '.bottom a');
    });

    $scope.animateVehicles = function () {
        var dist = Math.round((ve.frames.length - 1) / 20);

        if ($scope.region.id != 'london') { $('.bus').hide(); }

        ve.initElement($('#t1'), dist * 0 - 1);
        ve.initElement($('#t2'), dist * 1 - 1);
        ve.initElement($('#t3'), dist * 2 - 1);
        ve.initElement($('#t4'), dist * 3 - 1);
        ve.initElement($('#t5'), dist * 4 - 1);
        ve.initElement($('#t6'), dist * 5 - 1);
        ve.initElement($('#t7'), dist * 6 - 1);
        ve.initElement($('#t8'), dist * 7 - 1);
        ve.initElement($('#t9'), dist * 8 - 1);
        ve.initElement($('#t10'), dist * 9 - 1);
        ve.initElement($('#t11'), dist * 10 - 1);
        ve.initElement($('#t12'), dist * 11 - 1);
        ve.initElement($('#t13'), dist * 12 - 1);

        ve.animate($('#t1'), dist * 0);
        ve.animate($('#t2'), dist * 1);
        ve.animate($('#t3'), dist * 2);
        ve.animate($('#t4'), dist * 3);
        ve.animate($('#t5'), dist * 4);
        ve.animate($('#t6'), dist * 5);
        ve.animate($('#t7'), dist * 6);
        ve.animate($('#t8'), dist * 7);
        ve.animate($('#t9'), dist * 8);
        ve.animate($('#t10'), dist * 9);
        ve.animate($('#t11'), dist * 10);
        ve.animate($('#t12'), dist * 11);
        ve.animate($('#t13'), dist * 12);

        ve2.initElement($('#top_t1'), 0-1);
        ve2.initElement($('#top_t2'), 3-1);
        ve2.initElement($('#top_t3'), 5-1);
        ve2.initElement($('#top_t4'), 7-1);

        ve2.animate($('#top_t1'), 0);
        ve2.animate($('#top_t2'), 3);
        ve2.animate($('#top_t3'), 5);
        ve2.animate($('#top_t4'), 7);
    };

    $scope.animateVehicles();

}]);
