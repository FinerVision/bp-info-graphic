var Region = {

    regions: [
        {
            id: 'scotland',
            name: 'Scotland',
            x: 240, y: 174,
            data: {
                annual_contribution: {
                    value: 3,
                    unit: 'billion'
                },
                direct_contribution: {
                    value: 433.3,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 3.2,
                    unit: 'billion'
                },
                suppliers: {
                    value: 1070
                },
                induced_impact: {
                    value: 157.8,
                    unit: 'million'
                },
                jobs: {
                    value: 54406
                }
            }
        },
        {
            id: 'northern_ireland',
            name: 'Northern Ireland',
            x: 156, y: 332,
            data: {
                annual_contribution: {
                    value: 2.6,
                    unit: 'million'
                },
                direct_contribution: {
                    value: 1.7,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 2422
                },
                suppliers: {
                    value: 2
                },
                induced_impact: {
                    value: 1,
                    unit: 'million'
                },
                jobs: {
                    value: 42
                }
            }
        },
        {
            id: 'wales',
            name: 'Wales',
            x: 233, y: 458,
            data: {
                annual_contribution: {
                    value: 20.1,
                    unit: 'million'
                },
                direct_contribution: {
                    value: 3,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 17.5,
                    unit: 'million'
                },
                suppliers: {
                    value: 46
                },
                induced_impact: {
                    value: 6.3,
                    unit: 'million'
                },
                jobs: {
                    value: 437
                }
            }
        },
        {
            id: 'united_kingdom',
            name: 'United Kingdom',
            x: 220, y: 369,
            data: {
                annual_contribution: {
                    value: 9,
                    unit: 'billion'
                },
                direct_contribution: {
                    value: 2.3,
                    unit: 'billion'
                },
                indirect_impact: {
                    value: 7.8,
                    unit: 'billion'
                },
                suppliers: {
                    value: 4140
                },
                induced_impact: {
                    value: 752.1,
                    unit: 'million'
                },
                jobs: {
                    value: 132100
                }
            }
        },
        {
            id: 'north_west',
            name: 'North West',
            x: 275, y: 349,
            data: {
                annual_contribution: {
                    value: 661.7,
                    unit: 'million'
                },
                direct_contribution: {
                    value: 6.4,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 832.9,
                    unit: 'million'
                },
                suppliers: {
                    value: 325
                },
                induced_impact: {
                    value: 8.6,
                    unit: 'million'
                },
                jobs: {
                    value: 17189
                }
            }
        },
        {
            id: 'north_east',
            name: 'North East',
            x: 316, y: 339,
            data: {
                annual_contribution: {
                    value: 57.9,
                    unit: 'million'
                },
                direct_contribution: {
                    value: 12.6,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 64.6,
                    unit: 'million'
                },
                suppliers: {
                    value: 112
                },
                induced_impact: {
                    value: 13.5,
                    unit: 'million'
                },
                jobs: {
                    value: 1095
                }
            }
        },
        {
            id: 'yorkshire',
            name: 'Yorkshire',
            x: 315, y: 405,
            data: {
                annual_contribution: {
                    value: 234.3,
                    unit: 'million'
                },
                direct_contribution: {
                    value: 98.1,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 182.6,
                    unit: 'million'
                },
                suppliers: {
                    value: 244
                },
                induced_impact: {
                    value: 30.8,
                    unit: 'million'
                },
                jobs: {
                    value: 3874
                }
            }
        },
        {
            id: 'west_midlands',
            name: 'West Midlands',
            x: 279, y: 474,
            data: {
                annual_contribution: {
                    value: 185.6,
                    unit: 'million'
                },
                direct_contribution: {
                    value: 42.2,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 213.9,
                    unit: 'million'
                },
                suppliers: {
                    value: 167
                },
                induced_impact: {
                    value: 9.2,
                    unit: 'million'
                },
                jobs: {
                    value: 3375
                }
            }
        },
        {
            id: 'east_midlands',
            name: 'East Midlands',
            x: 336, y: 463,
            data: {
                annual_contribution: {
                    value: 111.2,
                    unit: 'million'
                },
                direct_contribution: {
                    value: 54,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 69.6,
                    unit: 'million'
                },
                suppliers: {
                    value: 176
                },
                induced_impact: {
                    value: 10,
                    unit: 'million'
                },
                jobs: {
                    value: 1737
                }
            }
        },
        {
            id: 'east',
            name: 'East',
            x: 376, y: 500,
            data: {
                annual_contribution: {
                    value: 414,
                    unit: 'million'
                },
                direct_contribution: {
                    value: 162.7,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 258.2,
                    unit: 'million'
                },
                suppliers: {
                    value: 275
                },
                induced_impact: {
                    value: 51,
                    unit: 'million'
                },
                jobs: {
                    value: 6273
                }
            }
        },
        {
            id: 'london',
            name: 'London',
            x: 343, y: 556,
            data: {
                annual_contribution: {
                    value: 2.1,
                    unit: 'billion'
                },
                direct_contribution: {
                    value: 540.8,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 1.6,
                    unit: 'billion'
                },
                suppliers: {
                    value: 802
                },
                induced_impact: {
                    value: 212.3,
                    unit: 'million'
                },
                jobs: {
                    value: 27438
                }
            }
        },
        {
            id: 'south_west',
            name: 'South West',
            x: 261, y: 554,
            data: {
                annual_contribution: {
                    value: 168.9,
                    unit: 'million'
                },
                direct_contribution: {
                    value: 69.6,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 102.5,
                    unit: 'million'
                },
                suppliers: {
                    value: 124
                },
                induced_impact: {
                    value: 22.9,
                    unit: 'million'
                },
                jobs: {
                    value: 2769
                }
            }
        },
        {
            id: 'south_east',
            name: 'South East',
            x: 374, y: 577,
            data: {
                annual_contribution: {
                    value: 2.1,
                    unit: 'billion'
                },
                direct_contribution: {
                    value: 843.9,
                    unit: 'million'
                },
                indirect_impact: {
                    value: 1.2,
                    unit: 'billion'
                },
                suppliers: {
                    value: 797
                },
                induced_impact: {
                    value: 228.8,
                    unit: 'million'
                },
                jobs: {
                    value: 30243
                }
            }
        }
    ]

};