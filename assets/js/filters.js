bp.filter('link', function () {
    return function (input) {
        return input.toLowerCase().split(' ').join('_');
    };
});

bp.filter('findRegion', function () {
    return function (input, id) {
        var length = input.length;

        for (var i = 0; i < length; i++) {
            if(input[i].id === id) {
                return input[i];
            }
        }

        return null;
    };
});