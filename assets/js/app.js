var bp = angular.module('bp', ['ngRoute', 'bpControllers']);

bp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.
        when('/:region', {
            templateUrl: 'views/region.html',
            controller: 'RegionController'
        }).
        otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true);
}]);